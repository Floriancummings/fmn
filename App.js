/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  StatusBar
} from 'react-native';
import Scenes from './src/scenes'


export default class App extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
      <StatusBar
       backgroundColor="#6A1B9A"
       barStyle="dark-content"
     />
        <Scenes/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});

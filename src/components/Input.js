'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TextInput
} from 'react-native';

class Input extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	isFocused:false
	  };
	}
	handleInputFocus = () => this.setState({ isFocused: true })

   handleInputBlur = () => this.setState({ isFocused: false })
  render() {
    return (
     	<TextInput 
     		style={styles.input}
     		onFocus={this.handleInputFocus}
     		onBlur={this.handleInputBlur}
     		placeholder={this.props.placeholder}
     		underlineColorAndroid={`${this.state.isFocused ? "#7B1FA2" :"#000000"}`}
     	/>
    );
  }
}

const styles = StyleSheet.create({
	input:{
		marginBottom: 10,
		height: 50,
		fontSize: 18

	}
});


export default Input;
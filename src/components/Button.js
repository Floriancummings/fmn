'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	View,
	TouchableOpacity,
	Dimensions,
	Text,
	Button
} from 'react-native';

class Buttont extends Component {
	render() {
		return (
			<TouchableOpacity activeOpacity={0.9}
			onPress={()=>{}}
			style={styles.button}
			>
			<Text style={styles.buttonText}>
			  {this.props.title}
			</Text>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
	button:{
		width:340,
		height:40,
		marginTop: 15,
		borderRadius:2,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#7B1FA2'
	},
	buttonText:{
		fontSize: 15,
		color:"white",
		fontWeight: 'bold' 
	}
});


export default Buttont;
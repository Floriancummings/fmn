'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	View,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import Auth from "./Auth";

const Scenes = StackNavigator({
	auth: {
		screen: Auth,
		navigationOptions:{
			title:"Log in"
		}
	},
},{
	navigationOptions:{
		headerStyle:{
			backgroundColor: '#7B1FA2',

		},
		headerTintColor:'#ffffff'
	}
})


export default Scenes;
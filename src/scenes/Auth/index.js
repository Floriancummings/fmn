'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import Input from "../../components/Input";
import Button from "../../components/Button";

class index extends Component {
  render() {
    return (
    	<ScrollView contentContainerStyle={{flex: 1}}>
      <KeyboardAvoidingView style={styles.container} behaviour={"height"}>
      	<View style={styles.header}>
      		<Text style={styles.logo}>
      		  LOGO
      		</Text>

      	</View>
      	<View style={styles.form}>
      		<Input placeholder={"Email"}/>
      		<Input placeholder={"Password"}/>
      		<Button title={"LOGIN"}/>
      		<Text style={styles.info}>
      		  Sign in with your company email address and assigned password
      		</Text>
      	</View>
      </KeyboardAvoidingView>
      </ScrollView>
      
    );
  }
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		
	},
	header:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		
	},
	form:{
		flex: 1,
		padding: 10,
		
	},
	logo:{
		fontSize: 50,
		fontWeight: '900',
		color:"black" 
	},
	info:{
		textAlign: 'center' , 
		padding: 10
	}
});


export default index;